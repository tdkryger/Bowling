﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Skat.Lib;
using Skat.Lib.Model;
using System.Collections.Generic;

namespace UtBusiness
{
    [TestClass]
    public class UtModel
    {
        // Some random testdata. Not all used
        private List<string> jsonStrings = new List<string>()
        {
            "{\"points\":[[6,2],[6,3]],\"token\":\"RNcqHDE0VqA8KotB1FywTiXKcPOUa6NL\"}",
"{\"points\":[[1,8],[9,1]],\"token\":\"9cpENaGb4VM8DexNa6ywoijBFZsYcwxa\"}",
"{\"points\":[[9,0],[3,2],[10,0],[2,7],[1,3],[3,1],[9,1]],\"token\":\"TqSi1w67jrX6HTogUbCMJ32xTTKFYsj9\"}",
"{\"points\":[[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,10]],\"token\":\"q5c3qdkhsncCp5CuEiQOXYo3xrRuhZzl\"}",
"{\"points\":[[3,7],[2,7],[6,1],[4,5],[2,2]],\"token\":\"TanwzdhVLwmieqQ9MTssLWMgAJdJ8koq\"}",
"{\"points\":[[8,1],[1,1],[0,0],[3,1],[10,0],[6,1],[6,2],[1,2]],\"token\":\"3YBX1sj3TYjT6QyqW9rSsNB9LBf3XGw1\"}",
"{\"points\":[[0,7],[5,1],[6,0],[6,4]],\"token\":\"qkG2PiVWiCR7wyEFPJCbbmjla5YLT2b2\"}",
"{\"points\":[[3,5],[7,0],[3,6],[2,2],[9,1],[6,3],[10,0],[0,6],[6,0],[9,0]],\"token\":\"0vG2PkEVYXr0tqKhWYYCbsPhdrMQbgbF\"}",
"{\"points\":[[7,0],[0,2],[3,5],[2,2],[5,2],[10,0],[6,1]],\"token\":\"ZSrOOXsgtdkNvYdtWs58xeAMqr0VL26g\"}",
"{\"points\":[[5,4],[2,0],[5,1],[2,4],[9,1],[6,3],[9,0],[6,2],[5,3],[8,2],[3,0]],\"token\":\"MzCizMWo8tuloFfwOmlAlp7Ru68XDjvl\"}",
"{\"points\":[[4,0],[9,1]],\"token\":\"UuaeFC4o2e1qQ1AKkms7DDyhX5MrDiye\"}",
"{\"points\":[[7,3],[5,5],[9,1]],\"token\":\"HMgaZS2WD8mUzuoJbuNMnGIt3NLYlV5y\"}",
"{\"points\":[[5,0],[10,0],[8,2]],\"token\":\"9iLCf3nZl3wY6b1ydxV5jjZUqb4Lsji8\"}",
"{\"points\":[[2,8],[10,0],[8,0],[10,0],[0,3]],\"token\":\"dIcIiFzqG1z9L5whLllxIAStFqbINRcj\"}",
"{\"points\":[[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,10]],\"token\":\"oseilef2Lu4Dmz8ZTq1D8f0nL2sFBRTn\"}",
"{\"points\":[[7,3],[0,2],[9,1]],\"token\":\"wVURQmVtyvth9E33re4bJFQYkFrwwDDJ\"}",
"{\"points\":[[0,7],[2,8],[8,2]],\"token\":\"Kgor5D4GI3Vdn03bbNwcMx6y5HYdDf5x\"}",
"{\"points\":[[3,7],[1,2],[10,0],[5,5],[9,0],[2,7],[4,3],[1,0]],\"token\":\"j0MJ5098YSkgYNSscQAmp6Kr09PRq8wi\"}",
"{\"points\":[[3,2],[0,5],[5,0],[2,6],[10,0]],\"token\":\"AA5uEYlAzdTQivUp6bivQb51qlgiMuDe\"}",
"{\"points\":[[2,8],[7,1],[2,5],[3,5],[6,1],[2,7],[1,5]],\"token\":\"b8KSdw0grHl497jbgsAPV6Xodq3Y7yg0\"}",

        };

        [TestMethod]
        public void Frames_PerfectScore()
        {
            Frame firstFrame = new Frame() { Ball1 = 10, Ball2 = 0 };
            Frame frame2 = new Frame(firstFrame) { Ball1 = 10, Ball2 = 0 };
            Frame frame3 = new Frame(frame2) { Ball1 = 10, Ball2 = 0 };
            Frame frame4 = new Frame(frame3) { Ball1 = 10, Ball2 = 0 };
            Frame frame5 = new Frame(frame4) { Ball1 = 10, Ball2 = 0 };
            Frame frame6 = new Frame(frame5) { Ball1 = 10, Ball2 = 0 };
            Frame frame7 = new Frame(frame6) { Ball1 = 10, Ball2 = 0 };
            Frame frame8 = new Frame(frame7) { Ball1 = 10, Ball2 = 0 };
            Frame frame9 = new Frame(frame8) { Ball1 = 10, Ball2 = 0 };
            Frame frame10 = new Frame(frame9) { Ball1 = 10, Ball2 = 0 };
            Frame frame11 = new Frame(frame10) { Ball1 = 10, Ball2 = 0 };
            Frame frame12 = new Frame(frame11) { Ball1 = 10, Ball2 = 0 };

            firstFrame.NextFrame = frame2;
            frame2.NextFrame = frame3;
            frame3.NextFrame = frame4;
            frame4.NextFrame = frame5;
            frame5.NextFrame = frame6;
            frame6.NextFrame = frame7;
            frame7.NextFrame = frame8;
            frame8.NextFrame = frame9;
            frame9.NextFrame = frame10;
            frame10.NextFrame = frame11;
            frame11.NextFrame = frame12;

            Assert.AreEqual(30, firstFrame.FramePoints, "First frame");
            Assert.AreEqual(60, frame2.FramePoints, "2nd frame");
            Assert.AreEqual(300, frame10.FramePoints, "Frame 10");
        }

        [TestMethod]
        public void Frames_PerfectScore_From_Json()
        {
            string json = "{\"points\":[[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,10]],\"token\":\"O5Em998G1ZVREuTraNehjmsRQ8jY3ZAr\"}";
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Frame firstFrame = new Frame(apiData);

            Assert.AreEqual(30, firstFrame.FramePoints, "First frame");
            Assert.AreEqual(60, firstFrame.NextFrame.FramePoints, "2nd frame");
            Assert.AreEqual(30, firstFrame.GetPointsFromFrame(1), "Frame 1");
            Assert.AreEqual(60, firstFrame.GetPointsFromFrame(2), "Frame 2");
            Assert.AreEqual(90, firstFrame.GetPointsFromFrame(3), "Frame 3");
            Assert.AreEqual(120, firstFrame.GetPointsFromFrame(4), "Frame 4");
            Assert.AreEqual(150, firstFrame.GetPointsFromFrame(5), "Frame 5");
            Assert.AreEqual(180, firstFrame.GetPointsFromFrame(6), "Frame 6");
            Assert.AreEqual(210, firstFrame.GetPointsFromFrame(7), "Frame 7");
            Assert.AreEqual(240, firstFrame.GetPointsFromFrame(8), "Frame 8");
            Assert.AreEqual(270, firstFrame.GetPointsFromFrame(9), "Frame 9");
            Assert.AreEqual(300, firstFrame.GetPointsFromFrame(10), "Frame 10");
            Assert.AreEqual(300, firstFrame.Total, "Total");
            Assert.AreEqual(firstFrame.GetPointsFromFrame(10), firstFrame.Total);
        }

        [TestMethod]
        public void Frames_From_Json()
        {
            string json = "{points: [[3,7],[10,0],[8,2],[8,1],[10,0],[3,4],[7,0],[5,5],[3,2],[2,5]], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";
            // 20,40,58,67,84,91,98,111,116,123
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Frame firstFrame = new Frame(apiData);

            Assert.AreEqual(20, firstFrame.FramePoints, "First frame");
            Assert.AreEqual(20, firstFrame.GetPointsFromFrame(1), "From frame 1");
            Assert.AreEqual(40, firstFrame.NextFrame.FramePoints, "2nd frame");
            Assert.AreEqual(40, firstFrame.GetPointsFromFrame(2), "From frame 2");
            Assert.AreEqual(58, firstFrame.GetPointsFromFrame(3), "From frame 3");
            Assert.AreEqual(67, firstFrame.GetPointsFromFrame(4), "From frame 4");
            Assert.AreEqual(84, firstFrame.GetPointsFromFrame(5), "From frame 5");
            Assert.AreEqual(91, firstFrame.GetPointsFromFrame(6), "From frame 6");
            Assert.AreEqual(98, firstFrame.GetPointsFromFrame(7), "From frame 7");
            Assert.AreEqual(111, firstFrame.GetPointsFromFrame(8), "From frame 8");
            Assert.AreEqual(116, firstFrame.GetPointsFromFrame(9), "From frame 9");
            Assert.AreEqual(123, firstFrame.GetPointsFromFrame(10), "From frame 10");
            Assert.AreEqual(123, firstFrame.Total, "Total");
            Assert.AreEqual(firstFrame.GetPointsFromFrame(10), firstFrame.Total);
        }

        [TestMethod]
        public void Frames_From_JsonString0()
        {
            string json = jsonStrings[0];
            
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Frame firstFrame = new Frame(apiData);

            OutgoingApiData outgoing = Outgoing.GetOutgoing(firstFrame, apiData.Token);

            Assert.AreEqual(8, firstFrame.FramePoints, "First frame");
            Assert.AreEqual(8, firstFrame.GetPointsFromFrame(1), "From frame 1");
            Assert.AreEqual(17, firstFrame.GetPointsFromFrame(2), "From frame 2");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(3), "From frame 3");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(4), "From frame 4");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(5), "From frame 5");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(6), "From frame 6");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(7), "From frame 7");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(8), "From frame 8");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(9), "From frame 9");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(10), "From frame 10");
            Assert.AreEqual(17, firstFrame.Total, "Total");

            Assert.AreEqual(17, outgoing.points[1]);
        }

        [TestMethod]
        public void Frames_From_JsonString1()
        {
            string json = jsonStrings[1];

            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Frame firstFrame = new Frame(apiData);

            OutgoingApiData outgoing = Outgoing.GetOutgoing(firstFrame, apiData.Token);

            Assert.AreEqual(9, firstFrame.FramePoints, "First frame");
            Assert.AreEqual(9, firstFrame.GetPointsFromFrame(1), "From frame 1");
            Assert.AreEqual(19, firstFrame.GetPointsFromFrame(2), "From frame 2");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(3), "From frame 3");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(4), "From frame 4");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(5), "From frame 5");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(6), "From frame 6");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(7), "From frame 7");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(8), "From frame 8");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(9), "From frame 9");
            Assert.AreEqual(0, firstFrame.GetPointsFromFrame(10), "From frame 10");
            Assert.AreEqual(19, firstFrame.Total, "Total");

            Assert.AreEqual(firstFrame.GetPointsFromFrame(1), outgoing.points[0]);
            Assert.AreEqual(firstFrame.GetPointsFromFrame(2), outgoing.points[1]);
        }
    }
}

