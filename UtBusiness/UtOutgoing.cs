﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Skat.Lib;
using Skat.Lib.Model;
using System;

namespace UtBusiness
{
    [TestClass]
    public class UtOutgoing
    {
        [TestMethod]
        public void Create_Valid_ApiData()
        {
            string json = "{points: [[3,7],[10,0],[8,2],[8,1],[10,0],[3,4],[7,0],[5,5],[3,2],[2,5]], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Frame frame = new Frame(apiData);

            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);

            //OutgoingApiData outgoingApiData = Outgoing.GetPoints(apiData);

            Assert.AreEqual("bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj", outgoingApiData.token);
        }

        [TestMethod]
        public void Calculate_Valid_Perfect_Score()
        {
            string json = "{points: [[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0]], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);

            Frame frame = new Frame(apiData);

            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);

            //OutgoingApiData outgoingApiData = Outgoing.GetPoints(apiData);

            Assert.AreEqual("bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj", outgoingApiData.token);
            Assert.AreEqual(300, outgoingApiData.points[9]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Should have thrown an ArgumentNullException")]
        public void Create_Null_ApiData()
        {
            ApiData apiData = null;
            Frame frame = new Frame(apiData);

            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);

            //OutgoingApiData outgoingApiData = Outgoing.GetPoints(apiData);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Should throw ArgumentException.")]
        public void Create_MissingToken_ApiData()
        {
            string json = "{points: [[6,2], [7,3],[4,3], [9,1], [6,0], [1,0], [1,7], [1,9] ]}";
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);

            Frame frame = new Frame(apiData);

            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);


            //OutgoingApiData outgoingApiData = Outgoing.GetPoints(apiData);
        }

        [TestMethod]
        public void Calculate_NoStrikeOrSpareInLast()
        {
            string expectedJson = "{\"token\":\"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\",\"points\":[20,40,58,67,84,91,98,111,116,123]}";
            string json = "{points: [[3,7],[10,0],[8,2],[8,1],[10,0],[3,4],[7,0],[5,5],[3,2],[2,5]], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";

            //OutgoingApiData expectedOutgoing = JsonConvert.DeserializeObject<OutgoingApiData>(expectedJson);
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);

            Frame frame = new Frame(apiData);

            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);


            //OutgoingApiData outgoingApiData = Outgoing.GetPoints(apiData);

            string jsonOut = JsonConvert.SerializeObject(outgoingApiData);

            Assert.AreEqual(apiData.Token, outgoingApiData.token, "Token");
            Assert.AreEqual(expectedJson, jsonOut);
        }

        [TestMethod]
        public void Calculate_StrikeInLast()
        {
            string expectedJson = "{\"token\":\"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\",\"points\":[20,40,58,67,84,91,98,111,116,133]}";
            string json = "{points: [[3,7],[10,0],[8,2],[8,1],[10,0],[3,4],[7,0],[5,5],[3,2],[10,0], [3,4]], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";

            //OutgoingApiData expectedOutgoing = JsonConvert.DeserializeObject<OutgoingApiData>(expectedJson);
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);

            //OutgoingApiData outgoingApiData = Outgoing.GetPoints(apiData);
            Frame frame = new Frame(apiData);

            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);


            string jsonOut = JsonConvert.SerializeObject(outgoingApiData);

            Assert.AreEqual(apiData.Token, outgoingApiData.token, "Token");
            Assert.AreEqual(expectedJson, jsonOut);
        }

        [TestMethod]
        public void Calculate_SpareInLast()
        {
            string expectedJson = "{\"token\":\"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\",\"points\":[20,40,58,67,84,91,98,111,116,129]}";
            string json = "{points: [[3,7],[10,0],[8,2],[8,1],[10,0],[3,4],[7,0],[5,5],[3,2],[7,3],[3,0]], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";

            //OutgoingApiData expectedOutgoing = JsonConvert.DeserializeObject<OutgoingApiData>(expectedJson);
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);

            //OutgoingApiData outgoingApiData = Outgoing.GetPoints(apiData);
            Frame frame = new Frame(apiData);

            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);


            string jsonOut = JsonConvert.SerializeObject(outgoingApiData);

            Assert.AreEqual(apiData.Token, outgoingApiData.token, "Token");
            Assert.AreEqual(expectedJson, jsonOut);
        }

        [TestMethod]
        public void Calculate_LessThan10Frames()
        {
            string expectedJson = "{\"token\":\"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\",\"points\":[20,40,58,67,0,0,0,0,0,0]}";
            string json = "{points: [[3,7],[10,0],[8,2],[8,1]], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";

            //OutgoingApiData expectedOutgoing = JsonConvert.DeserializeObject<OutgoingApiData>(expectedJson);
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);

            //OutgoingApiData outgoingApiData = Outgoing.GetPoints(apiData);

            Frame frame = new Frame(apiData);

            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);


            string jsonOut = JsonConvert.SerializeObject(outgoingApiData);

            Assert.AreEqual(apiData.Token, outgoingApiData.token, "Token");
            Assert.AreEqual(expectedJson, jsonOut);
        }
    }
}
