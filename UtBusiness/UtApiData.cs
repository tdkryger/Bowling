﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Skat.Lib;

namespace UtBusiness
{
    [TestClass]
    public class UtApiData
    {
        [TestMethod]
        public void Get_ApiData_From_Valid_Json()
        {
            string json = "{points: [[6,2], [7,3],[4,3], [9,1], [6,0], [1,0], [1,7], [1,9] ], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Assert.AreEqual("bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj", apiData.Token);
            Assert.AreEqual(8, apiData.Points.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(JsonReaderException))]
        public void Get_ApiData_From_Invalid_Json()
        {
            string json = "{points: [[6,2], [7,3],[4,3], [9,1], [6,0], [1,0], [1,7], [1,9 ], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Assert.AreEqual("bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj", apiData.Token);
            Assert.AreEqual(8, apiData.Points.Count);
        }

        [TestMethod]
        public void Get_ApiData_From_Empty_Json()
        {
            string json = string.Empty;
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Assert.IsNull(apiData);
        }
    }
}
