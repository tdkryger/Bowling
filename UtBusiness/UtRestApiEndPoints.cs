﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Skat.Lib;
using Skat.Lib.Model;
using System.Net;

namespace UtBusiness
{
    [TestClass]
    public class UtRestApiEndPoints
    {
        [TestMethod]
        public void GetPointSet()
        {
            string notExpected = string.Empty;
            string json = string.Empty;
            RestApiEndPoints endPoints = new RestApiEndPoints();

            json = endPoints.GetBowlingData().Result;

            Assert.AreNotEqual(notExpected, json);
        }

        [TestMethod]
        public void PostToEndPoint_ValidData()
        {
            string json = string.Empty;
            HttpStatusCode expectedStatusCode = HttpStatusCode.OK;
            PostResult httpStatus;

            RestApiEndPoints endPoints = new RestApiEndPoints();
            json = endPoints.GetBowlingData().Result;
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Frame frame = new Frame(apiData);
            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);
            httpStatus = endPoints.PostPoints(outgoingApiData).Result;

            Assert.IsFalse(string.IsNullOrEmpty(httpStatus.TextResult));
            Assert.AreEqual(expectedStatusCode, httpStatus.StatusCode);
        }

        [TestMethod]
        public void PostToEndPoint_InvalidToken()
        {
            HttpStatusCode notExpected = HttpStatusCode.OK;
            PostResult httpStatus;
            string json = "{points: [[3,7],[10,0],[8,2],[8,1],[10,0],[3,4],[7,0],[5,5],[3,2],[2,5]], token: \"bAyh4khi1guWt8Q5hqdeSgXbbOe3OSgj\"}";
            ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
            Frame frame = new Frame(apiData);
            OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);
            RestApiEndPoints endPoints = new RestApiEndPoints();

            httpStatus = endPoints.PostPoints(outgoingApiData).Result;

            Assert.IsFalse(string.IsNullOrEmpty(httpStatus.TextResult));
            Assert.AreNotEqual(notExpected, httpStatus.StatusCode);
        }
    }
}
