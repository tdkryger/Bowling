﻿using Skat.Lib.Model;
using System;

namespace Skat.Lib
{
    /// <summary>
    /// A static class to transform a Frame to a class used to create json
    /// </summary>
    public static class Outgoing
    {
        #region Public methods

        /// <summary>
        /// Creates an intermediate object for transport to the API
        /// </summary>
        /// <param name="firstFrame">The first frame in the game</param>
        /// <param name="token">The token recieved from the API</param>
        /// <returns></returns>
        public static OutgoingApiData GetOutgoing(Frame firstFrame, string token)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Token must not be empty or null");
            OutgoingApiData outgoing = new OutgoingApiData() { token = token };
            outgoing.points[0] = firstFrame.FramePoints;
            // Remember frames are 1-based
            for (int i = 1; i < 10; i++)
                outgoing.points[i] = firstFrame.GetPointsFromFrame(i+1);
            
            return outgoing;
        }
        #endregion
    }
}
