﻿using System.Collections.Generic;


namespace Skat.Lib
{
    /// <summary>
    /// Class used to convert incomming json to a class
    /// </summary>
    public class ApiData
    {
        /// <summary>
        /// The points for the game
        /// </summary>
        public List<int[]> Points { get; set; } = new List<int[]>();
        /// <summary>
        /// Token for the API
        /// </summary>
        public string Token { get; set; }
    }
}
