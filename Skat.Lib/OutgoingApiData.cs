﻿namespace Skat.Lib
{
    /// <summary>
    /// Data class to hold outgoing data to the API.
    /// Properties needs to be lower case for post to work.
    /// </summary>
    public class OutgoingApiData
    {
        /// <summary>
        /// Token from the get from the API
        /// </summary>
        public string token { get; set; }
        /// <summary>
        /// The points in the game
        /// </summary>
        public int[] points { get; private set; } = new int[10];
    }
}
