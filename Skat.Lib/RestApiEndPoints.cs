﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Skat.Lib
{
    //TODO: Static class?
    /// <summary>
    /// Class with the API endpoints.
    /// </summary>
    public class RestApiEndPoints
    {
        /// <summary>
        /// Retrieves json from the API.
        /// Uri is hardcoded
        /// </summary>
        /// <returns>The json</returns>
        public async Task<string> GetBowlingData()
        {
            HttpClient client = GetHttpClient("http://13.74.31.101/api/points");

            HttpResponseMessage httpResponse = await client.GetAsync(string.Empty);
            if (httpResponse.IsSuccessStatusCode)
            {
                return await httpResponse.Content.ReadAsStringAsync();
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// Post the calculated points from a game
        /// </summary>
        /// <param name="outgoing">The dataclass containing the points</param>
        /// <returns></returns>
        public async Task<PostResult> PostPoints(OutgoingApiData outgoing)
        {
            HttpClient client = GetHttpClient("http://13.74.31.101/api/points");

            HttpResponseMessage httpResponse = await client.PostAsJsonAsync<OutgoingApiData>(string.Empty, outgoing);
            string json = string.Empty;
            json = await httpResponse.Content.ReadAsStringAsync();
            return new PostResult(httpResponse.StatusCode, json);
        }

        #region Private methods
        private HttpClient GetHttpClient(string url)
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(url)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
        #endregion
    }

    /// <summary>
    /// Holder-class used when posting data to the API
    /// </summary>
    public class PostResult
    {
        /// <summary>
        /// The HTTP Status Code
        /// </summary>
        public HttpStatusCode StatusCode { get; protected set; }
        /// <summary>
        /// Any text from posting to the API
        /// </summary>
        public string TextResult { get; protected set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="statusCode">The status code</param>
        /// <param name="textResult">The result</param>
        public PostResult(HttpStatusCode statusCode, string textResult)
        {
            this.StatusCode = statusCode;
            this.TextResult = textResult;
        }
    }
}
