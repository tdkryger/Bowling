﻿using System;

namespace Skat.Lib.Model
{
    /// <summary>
    /// Frame to hold results from a bowling match. 
    /// </summary>
    public class Frame : IDisposable
    {
        private Frame _priorFrame;

        /// <summary>
        /// The first ball in the frame
        /// </summary>
        public int Ball1 { get; set; }
        /// <summary>
        /// The second ball in the frame
        /// </summary>
        public int Ball2 { get; set; }
        /// <summary>
        /// True if the frame have a strike
        /// </summary>
        public bool IsStrike { get { return GetIsStrike(); } }
        /// <summary>
        /// True if the frame have a spare
        /// </summary>
        public bool IsSpare { get { return GetIsSpare(); } }
        /// <summary>
        /// The accumulated points for this frame
        /// </summary>
        public int FramePoints { get { return GetFramePoint(); } }
        /// <summary>
        /// The next frame in the game
        /// </summary>
        public Frame NextFrame { get; set; }
        /// <summary>
        /// The prior frame in the game
        /// </summary>
        public Frame PriorFrame { get { return _priorFrame; } }
        /// <summary>
        /// Finds the last frame with a score above 0, and returns the value
        /// </summary>
        public int Total { get { return GetTotal(); } }

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public Frame()
        {
            _priorFrame = null;
        }

        /// <summary>
        /// Constructor with a parameter for the prior frame in the game
        /// </summary>
        /// <param name="prior">The prior frame</param>
        public Frame(Frame prior)
        {
            _priorFrame = prior;
        }

        /// <summary>
        /// Creates the game from the data from the API
        /// </summary>
        /// <param name="apiData">The source of the game</param>
        public Frame(ApiData apiData)
        {
            if (apiData == null)
                throw new ArgumentNullException();
            Frame priorFrame = null;

            Frame currentFrame = this;
            this.Ball1 = apiData.Points[0][0];
            this.Ball2 = apiData.Points[0][1];

            for (int i = 1; i < apiData.Points.Count; i++)
            {
                priorFrame = currentFrame;
                currentFrame = new Frame(priorFrame);
                if (priorFrame != null)
                    priorFrame.NextFrame = currentFrame;
                currentFrame.Ball1 = apiData.Points[i][0];
                currentFrame.Ball2 = apiData.Points[i][1];
            }
        }

        /// <summary>
        /// Returns the score from a frame counted from the current one, or 0 if the frame does not exists
        /// </summary>
        /// <param name="frameNumber">The framenumber. This frame is 0</param>
        /// <returns>The score, or 0 if the frame is null</returns>
        public int GetPointsFromFrame(int frameNumber)
        {
            Frame theFrame = this;
            for (int i = 0; i < frameNumber - 1; i++)
            {
                if (theFrame.NextFrame != null)
                    theFrame = theFrame.NextFrame;
                else
                    return 0;
            }
            return theFrame.FramePoints;
        }

        #region Private methods
        private int GetTotal()
        {
            int idx = 10;
            int value = 0;
            while (value == 0)
            {
                value = GetPointsFromFrame(idx);
                idx--;
            }
            return value;
        }

        private int GetFramePoint()
        {
            int sum = Ball1 + Ball2;
            if (_priorFrame != null)
                sum = sum + _priorFrame.FramePoints;

            if (NextFrame != null)
            {
                if (IsStrike)
                {
                    sum = sum + NextFrame.Ball1 + NextFrame.Ball2;
                    if (NextFrame.IsStrike)
                    {
                        if (NextFrame.NextFrame != null)
                        {
                            if (NextFrame.NextFrame.Ball1 == 10 && NextFrame.NextFrame.Ball2 == 10)
                            {
                                sum = sum + 10;
                            }
                            else
                            {
                                sum = sum + NextFrame.NextFrame.Ball1 + NextFrame.NextFrame.Ball2;
                            }
                        }
                    }
                }
                else
                {
                    if (IsSpare)
                    {
                        sum = sum + NextFrame.Ball1;
                    }
                }
            }
            return sum;
        }

        private bool GetIsStrike()
        {
            if (Ball1 == 10 && Ball2 == 0)
                return true;
            else
                return false;
        }

        private bool GetIsSpare()
        {
            if (((Ball1 + Ball2) == 10) && (!GetIsStrike()))
                return true;
            else
                return false;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Dispose the frame. Disposes NextFrame depinding on the parameter
        /// </summary>
        /// <param name="disposing">If true, disposes NextFrame</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (NextFrame != null)
                    {
                        NextFrame.Dispose();
                        NextFrame = null;
                    }
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// Dispose this, and NextFrame
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #endregion
    }
}
