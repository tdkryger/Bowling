﻿using Newtonsoft.Json;
using Skat.Lib;
using Skat.Lib.Model;
using System;
using System.Collections.Generic;

namespace BowlingGUI
{
    class Program
    {
        static void Main(string[] args)
        {
            int loopCount = 20;
            RestApiEndPoints endPoints = new RestApiEndPoints();

            while (loopCount > 0)
            {
                string json = endPoints.GetBowlingData().Result;
                ApiData apiData = JsonConvert.DeserializeObject<ApiData>(json);
                Frame frame = new Frame(apiData);
                Console.ResetColor();
                Console.WriteLine();
                Console.WriteLine("Input:");
                Console.Write("Json: "); Console.WriteLine(json);
                Console.Write("Points:   |");
                Console.Write(frame.Ball1.ToString().PadLeft(2, ' '));
                Console.Write(", ");
                Console.Write(frame.Ball2.ToString().PadLeft(2, ' '));
                Console.Write(" | ");
                for (int i = 1; i < apiData.Points.Count; i++)
                {
                    Console.Write(frame.NextFrame.Ball1.ToString().PadLeft(2, ' '));
                    Console.Write(", ");
                    Console.Write(frame.NextFrame.Ball2.ToString().PadLeft(2, ' '));
                    Console.Write(" | ");
                }
                Console.WriteLine();

                OutgoingApiData outgoingApiData = Outgoing.GetOutgoing(frame, apiData.Token);

                Console.Write("Resultat: |");
                for (int i = 0; i < outgoingApiData.points.Length; i++)
                {
                    Console.Write(outgoingApiData.points[i].ToString().PadLeft(6, ' '));
                    Console.Write(" | ");
                }
                Console.WriteLine();

                string jsonOutgoing = JsonConvert.SerializeObject(outgoingApiData);


                PostResult httpStatus = endPoints.PostPoints(outgoingApiData).Result;
                if (httpStatus.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.Write("Post result: ");
                Console.Write(httpStatus.StatusCode.ToString());
                Console.Write(", ");
                Console.WriteLine(httpStatus.TextResult);

                Console.WriteLine();
                Console.WriteLine();
                loopCount--;
            }
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Press enter to exit.");
            Console.ReadLine();
        }

        static List<int> GetNext(List<int> list, int start, int count)
        {
            if ((start + count) > list.Count)
            {
                return GetNext(list, start, count - 1);
            }
            else
                return list.GetRange(start, count);
        }
    }
}
